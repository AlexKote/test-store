import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ChangeValue, ResetValues } from '../store/app.actions';
import { selectFirstValue, selectSecondValue } from '../store/app.selectors';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-store-ui',
  templateUrl: './store-ui.component.html',
  styleUrls: ['./store-ui.component.css'],
})
export class StoreUiComponent implements OnInit, OnDestroy {
  public value1: number;
  public value2: number;
  public isNegativeValue1: boolean;
  public isNegativeValue2: boolean;
  public isStarted: boolean;
  private interval: Subscription;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.isStarted = false;
    this.store.pipe(select(selectFirstValue)).subscribe((vl) => {
      this.value1 = vl;
      this.isNegativeValue1 = this.value1 < 0 ? true : false;
    });
    this.store.pipe(select(selectSecondValue)).subscribe((vl) => {
      this.value2 = vl;
      this.isNegativeValue2 = this.value2 < 0 ? true : false;
    });
  }

  onStart(): void {
    this.isStarted = true;
    this.interval = interval(1000).subscribe((vl) =>
      this.store.dispatch(new ChangeValue())
    );
  }

  onStop(): void {
    if (this.isStarted) {
      this.interval.unsubscribe();
      this.isStarted = false;
    }
  }

  onReset(): void {
    this.onStop();
    this.store.dispatch(new ResetValues());
  }

  ngOnDestroy(): void {
    this.onStop();
  }
}
