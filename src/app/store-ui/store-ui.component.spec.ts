import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUiComponent } from './store-ui.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { appReducer, initialAppState } from '../store/app.reducer';
import { IValueState } from '../store/app.selectors';

describe('StoreUiComponent', () => {
  let component: StoreUiComponent;
  let fixture: ComponentFixture<StoreUiComponent>;
  let initialState: IValueState;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StoreUiComponent],
      imports: [FormsModule, StoreModule.forRoot({ numbers: appReducer })],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUiComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    initialState = initialAppState;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be display store values', () => {
    expect(component.value1).toBe(initialState.firstValue);
    expect(component.value2).toBe(initialState.secondValue);
  });

  it('values should be red and < 0', () => {
    component.value1 = -5;
    component.value2 = -5;
    component.isNegativeValue1 = component.value1 < 0 ? true : false;
    component.isNegativeValue2 = component.value2 < 0 ? true : false;
    fixture.detectChanges();
    expect(compiled.querySelector('#value1')).toHaveClass('redColor');
    expect(compiled.querySelector('#value2')).toHaveClass('redColor');
  });

  it('values should be blue and >= 0', () => {
    component.value1 = 10;
    component.value2 = 10;
    component.isNegativeValue1 = component.value1 < 0 ? true : false;
    component.isNegativeValue2 = component.value2 < 0 ? true : false;
    fixture.detectChanges();
    expect(
      compiled.querySelector('#value1').classList.contains('redColor')
    ).toBeFalse();
    expect(
      compiled.querySelector('#value2').classList.contains('redColor')
    ).toBeFalse();
  });
});
