import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, tap } from 'rxjs/operators';
import { EAppActions, IncreaseFirstValue, DecreaseSecondValue } from './app.actions';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';


@Injectable()
export class AppEffects {
  constructor(private actions$: Actions) {}

  @Effect()
  changeValue$: Observable<Action> = this.actions$.pipe(
    tap(),
    ofType(EAppActions.ChangeValue),
    switchMap(() => {
      return [new IncreaseFirstValue(), new DecreaseSecondValue(), new DecreaseSecondValue()];
    })
  );
}
