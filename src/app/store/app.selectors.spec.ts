import {
  IAppState,
  selectFirstValue,
  selectSecondValue,
} from './app.selectors';
import { initialAppState } from './app.reducer';

describe('UserSelectors', () => {
  const state: IAppState = {
    numbers: initialAppState,
  };
  const key = 'numbers';

  describe('Number Selectors', () => {
    it('selectFirstValue should return first value', () => {
      const results = selectFirstValue(state);
      expect(results).toBeTruthy();
      expect(results).toBe(initialAppState.firstValue);
    });

    it('selectSecondValue should return second value', () => {
      const results = selectSecondValue(state);
      expect(results).toBeTruthy();
      expect(results).toBe(initialAppState.secondValue);
    });
  });
});
