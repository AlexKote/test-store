import { createSelector } from '@ngrx/store';

export interface IValueState {
  firstValue: number;
  secondValue: number;
}
export interface IAppState {
  numbers: IValueState;
}

export const selectValueState = (state: IAppState) => state.numbers;

export const selectFirstValue = createSelector(
  selectValueState,
  (state: IValueState) => state.firstValue
);
export const selectSecondValue = createSelector(
  selectValueState,
  (state: IValueState) => state.secondValue
);
