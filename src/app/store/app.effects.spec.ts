import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store, StoreModule, createAction } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { cold, hot } from '@nrwl/angular/testing';
import { Observable, of } from 'rxjs';

// import { apolloErrorStub } from '@medium-stories/common/testing';
// import { actionPropsForcePayloadStub, setMockStore } from '@medium-stories/store/testing';
// import { UserApollo } from '@medium-stories/users';

// import { userStub } from '../../../testing';
// import * as UserActions from './user.actions';
import { AppEffects } from './app.effects';
import { initialAppState, appReducer } from './app.reducer';
import { IValueState } from './app.selectors';
import {
  EAppActions,
  IncreaseFirstValue,
  DecreaseSecondValue,
  ChangeValue,
} from './app.actions';
import { EffectsModule, Actions, ofType } from '@ngrx/effects';
import { Action } from 'rxjs/internal/scheduler/Action';

describe('AppEffects', () => {
  let actions: Observable<any>;
  let effects: AppEffects;
  let store: Store;
  let mockValueState: IValueState = {
    firstValue: -5,
    secondValue: 10,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      //   imports: [NxModule.forRoot()],
      imports: [
        StoreModule.forRoot({ numbers: appReducer }),
        EffectsModule.forRoot([AppEffects]),
      ],
      //   providers: [
      //     AppEffects,
      //     // DataPersistence,
      //     provideMockActions(() => actions),
      //     provideMockStore({
      //       initialState: { numbers: initialAppState }
      //     })
      //   ]
    });

    effects = TestBed.inject(AppEffects);
    store = TestBed.inject(Store);
  });

  // describe('ChangeValue$', () => {
    // it('should call IncreaseFirstValue() once and DecreaseSecondValue twice', () => {
      // const action = new IncreaseFirstValue();
      // const expected = of(
      // { type: '[App] Increase first value'}
      // new IncreaseFirstValue(),
      // new DecreaseSecondValue(),
      // new DecreaseSecondValue(),
      // );
      //   const expected = new IncreaseFirstValue();

      // actions = hot('-a-|', { a: new ChangeValue() });
      // const expected = hot('-a-b-c-|', {
      //   a: new IncreaseFirstValue(),
      //   b: new DecreaseSecondValue(),
      //   c: new DecreaseSecondValue(),
      // });
      // console.log(actions);
      // console.log(expected);
      // console.log(effects.changeValue$);
      // expect(effects.changeValue$).toBeObservable(expected);
      // expect(effects.changeValue$).toEqual(expected);
    // });

    // it('should call load user cancel', () => {
    //   setMockStore(store, key, userInitialState, { userLoadRun: true });
    //   actions = hot('-a-|', { a: UserActions.loadUser(actionPropsForcePayloadStub) });
    //   const expected = hot('-a-|', { a: UserActions.loadUserCancel() });

    //   expect(effects.loadUser$).toBeObservable(expected);
    // });
  // });

  //   describe('loadUserRun$', () => {
  //     it('should call load user success', () => {
  //       actions = hot('-a-|', { a: UserActions.loadUserRun() });
  //       const response = cold('-a|', { a: userStub });
  //       const expected = hot('--a|', { a: UserActions.loadUserSuccess({ payload: userStub }) });
  //       apollo.loadUser = jest.fn(() => response);

  //       expect(effects.loadUserRun$).toBeObservable(expected);
  //     });

  //     it('should call load user error', () => {
  //       actions = hot('-a-|', { a: UserActions.loadUserRun() });
  //       const response = cold('-#|', {}, apolloErrorStub);
  //       const expected = hot('--a|', { a: UserActions.loadUserFailure({ payload: apolloErrorStub }) });
  //       apollo.loadUser = jest.fn(() => response);

  //       expect(effects.loadUserRun$).toBeObservable(expected);
  //     });
  //   });
});
