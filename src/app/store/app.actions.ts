import { Action } from '@ngrx/store';

export enum EAppActions {
  ChangeValue = '[App] Change',
  IncreaseFirstValue = '[App] Increase first value',
  DecreaseSecondValue = '[App] Decrease second value',
  ResetValues = '[App] Reset all values',
}

export class ChangeValue implements Action {
  readonly type = EAppActions.ChangeValue;
}
export class IncreaseFirstValue implements Action {
  readonly type = EAppActions.IncreaseFirstValue;
}
export class DecreaseSecondValue implements Action {
  readonly type = EAppActions.DecreaseSecondValue;
}
export class ResetValues implements Action {
  readonly type = EAppActions.ResetValues;
}

export type AppActions =
  | ChangeValue
  | IncreaseFirstValue
  | DecreaseSecondValue
  | ResetValues;
