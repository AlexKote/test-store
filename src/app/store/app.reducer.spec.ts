import {
  ResetValues,
  IncreaseFirstValue,
  DecreaseSecondValue,
  ChangeValue,
} from './app.actions';
import { IValueState } from './app.selectors';
import { initialAppState, appReducer } from './app.reducer';

describe('AppReducers', () => {
  let state: IValueState;

  beforeEach(() => {
    state = initialAppState;
  });

  describe('valid App actions', () => {
    it('Reset() should reset values by {-5, 10}', () => {
      const action = new ResetValues();
      const result = appReducer(state, action);
      expect(result.firstValue).toBe(-5);
      expect(result.secondValue).toBe(10);
    });

    it('IncreaseFirstValue() should increase first variable by 1 (= -4)', () => {
      const action = new IncreaseFirstValue();
      const result = appReducer(state, action);
      expect(result.firstValue).toBe(state.firstValue + 1);
    });

    it('DecreaseSecondValue() should decrease second variable by 1 (= 9)', () => {
      const action = new DecreaseSecondValue();
      const result = appReducer(state, action);
      expect(result.secondValue).toBe(state.secondValue - 1);
    });

    it('ChangeValue() should do nothing in reducer', () => {
      const action = new ChangeValue();
      const result = appReducer(state, action);
      expect(result).toEqual(state);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = appReducer(initialAppState, action);
      expect(result).toEqual(initialAppState);
    });
  });
});
