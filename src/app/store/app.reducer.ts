import { IValueState } from './app.selectors';
import { AppActions, EAppActions } from './app.actions';

export const initialAppState: IValueState = {
  firstValue: -5,
  secondValue: 10,
};

export function appReducer(state = initialAppState, action: AppActions) {
  switch (action.type) {
    case EAppActions.ChangeValue: {
      state = {
        ...state,
      };
      break;
    }
    case EAppActions.IncreaseFirstValue: {
      state = {
        ...state,
        firstValue: state.firstValue + 1,
      };
      break;
    }
    case EAppActions.DecreaseSecondValue: {
      state = {
        ...state,
        secondValue: state.secondValue - 1,
      };
      break;
    }
    case EAppActions.ResetValues: {
      state = {
        ...initialAppState
      };
      break;
    }
  }
  return state;
}
