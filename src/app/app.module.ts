import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { StoreUiComponent } from './store-ui/store-ui.component';
import { InputsComponent } from './inputs/inputs.component';
import { appReducer } from './store/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './store/app.effects';

@NgModule({
  declarations: [AppComponent, StoreUiComponent, InputsComponent],
  imports: [
    BrowserModule,
    FormsModule,
    EffectsModule.forRoot([AppEffects]),
    StoreModule.forRoot({ numbers: appReducer })
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
