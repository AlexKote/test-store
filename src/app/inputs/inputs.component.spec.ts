import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputsComponent } from './inputs.component';
import { FormsModule } from '@angular/forms';

describe('InputsComponent', () => {
  let component: InputsComponent;
  let fixture: ComponentFixture<InputsComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputsComponent],
      imports: [FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('positive number should be blue', () => {
    component.inputValue = 10;
    component.onChange();
    fixture.detectChanges();
    expect(compiled.querySelector('.redColor')).toBeNull();
  });

  it('negative number should be red', () => {
    component.inputValue = -5;
    component.onChange();
    fixture.detectChanges();
    expect(compiled.querySelector('.redColor')).toBeTruthy();
  });
});
