import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.css'],
})
export class InputsComponent implements OnInit {
  public inputValue: number;
  public isNegative: boolean;

  constructor() {}

  ngOnInit(): void {
    this.isNegative = false;
    this.inputValue = 0;
  }
  

  onChange(): void {
    this.isNegative = this.inputValue < 0 ? true : false;
    if (this.inputValue === null) {
      this.inputValue = 0;
    }
  }
}
